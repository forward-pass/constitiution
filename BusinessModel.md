# Forward Pass Business Model

## Summary
Forward Pass aims to create value by producing and selling robotic hardware and software.
Specifically, using open-sourced technology and modifying it to decrease the cost even further, we can reach a much wider market.
Pairing the low-cost design with heavily automating and optimizing our robotics production process, we can deliver robotic hardware at a fraction of the cost of current competitors.
As hardware becomes cheaper and more available from more companies, Forward Pass will sustain itself on software sales, investing the proceeds into making even smarter software available for wider and wider audiences.


## Phase I
Initially, we will perform an end-to-end service, building hardware and implementing it for our clients.
Ideally, we would sell a subscription for updates and maintenance for open-source software and hardware.
A subscription to our service pays for continual optimization and iteration to improve our first clients' productivity.
At some point, we hope to have developed enough tools and training that our customers can maintain their own robots and even reconfigure robots for new applications without our involvement.
This will allow us to reach more people and move on to a new business model.

## Phase II
As we build hardware production capacity, refine our designs, and add more intelligence to our software, we hope to transition to something akin to an 'operating system' for robotics: A set of tools and a marketplace for people to sell robotic software and hardware.
As the hardware becomes cheaper and more available (as technology and mass-production always allow), we will invest fully into making sure our software platform can run on all of the exciting and improved variations of hardware.

At this stage, we will continue to build, improve, and support our general 'operating system' for robotics. 
We will sell clients monthly subscriptions for the software that brings their low-cost robots to life.
We will invest in tools that every robot-user will want to use to make the process of developing new applications easier, faster, and safer.
We will invest in creating domestic routines that run on our platform (e.g. 'pouring cereal' or 'cleaning rooms'), allowing our software to be marked to a much broader audience.
Cheaper hardware will become increasingly capable, and our software will leverage all of this hardware to improve everyone's lives for a small subscription fee per routine.

## Phase III
In our third phase, we will transition to primarily running our marketplace: a single location where anyone may go to purchase robotic hardware or download any robotic software from free room-cleaning rover routines to expensive, cutting-edge humanoid robot control software maintained by top roboticists.
We will continue to focus on improving developer tools and making robotics use even more accessible to all people.
Simultaneously, will use some proceeds from sales to invest in improving hardware designs and our own software routines for sale.
We will aim to incorporate every advance that comes to the robotics and AI fields and make sure they are available in our marketplace, charging a small fee for every transaction and investing the proceeds back into development of new products and the improvement of social conditions around the world.

## Conclusions
The tools we produce are going to change the world.
It is our responsibility to make sure everyone can benefit from this boom of technological change.
It is our responsibility to make sure robots elevate the condition of humanity rather than continue to enforce existing social hierarchies.
