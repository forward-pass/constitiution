# Constitution
## Preamble

We have all experienced the company in which we have absolutely no control. 
We have witnessed decisions made from the top, not caring about the feasibility, effectiveness, or correctness of what is requested.
We have witnessed the tyranny of the value we produce going to the uninterested, unaccountable shareholder while we are afforded an infinitesimal fraction of our worth.

We will create a better system which puts control in the hands of those who create value. 
This system will allow us to control what we do with the value we produce.
This system will become a template for a better world in which everyone has a vote in the most important decisions.

## Rules
### One person, one share, one vote

* You become a member of the company by majority vote.
* The only way for you to receive a share is by becoming a member of the company.
* You cannot transfer a share to another party.
* Leaving or being removed from the company destroys your share.
* You leave the company in any of these situations:
    1. You quit
    2. You are voted out by a majority vote
    3. You die
* A share allows you to vote in company decisions.

### Money
* 5% of Revenue goes to charitable causes chosen by vote.
* A percent of would-be Profit, chosen by vote, goes to Base pay for everyone.
* A percent of would-be Profit, chosen by vote, goes to Bonus pay for everyone.
* A percent of would-be Profit, chosen by vote, goes to research and development project funding.

* There are two categories of pay:
    * Base
    * Bonus

* Base pay is the same among everyone in the company.

* Bonus pay is determined democratically:
    * You have 100 bonus points to give to others based on how much you think they have helped the company.
    * You cannot distribute bonus points to yourself.
    * You receive bonus money based on the number of bonus points you receive:
        * (bonus pay) = (your received bonus points) / (total bonus points received by everyone)

### Votes
* Votes are private
* Votes are ranked-choice when a problem has multiple options
* Dates for annual votes and initial money allocation are chosen by an initial vote.
