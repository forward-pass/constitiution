# Forward Pass, LLC.
Welcome To the Forward Pass Company repository. Here you will find founding documents and core values of our company. We hope this collection of information will help others start companies with a similar structure to this one. We also hope you find value in our mission and help us bring about a better world for all.

## Core Goals
### 1. Free all of humanity from the burdon of forced labor
Automation should serve all of humanity. All human life is equally precious and worth nurturing and growing into its best form. All people should be free to fulfull their potential and pursue their dreams without suffering the exploitation and slavery of wage-work for an employer.

### 2. Automate everything
We seek to free humanity to pursue higher goals than finding the next meal. Automation is the only way to accomplish this liberation for all.

### 3. Appreciate different approaches, ideas, and perspectives
To have a different idea is not necessarily to be wrong. The only way to know if an idea is good or bad is to experiment. If we see something that does not work, we have an obligation to change it. This is the only way we can succeed at our mission.

## Documents

[Constitution](https://gitlab.com/forward-pass/constitiution/-/blob/main/Constitution.md) - our company rules and regulations

[Business Model](https://gitlab.com/forward-pass/constitiution/-/blob/main/BusinessModel.md) - How we bring about a better world

